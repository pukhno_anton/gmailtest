package com.gmail.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by apuhno on 17.06.2016.
 */
public class LoggedInPage extends BasePage {

    public LoggedInPage() {
        PageFactory.initElements(BaseTest.driver(), this);
    }

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
    private WebElement writeButton;

    @FindBy(xpath = "//textarea[@class='vO']")
    private WebElement destinationInput;

    @FindBy(xpath = "//span[@class='aB gQ pE']")
    private WebElement addUserInCopySpan;

    @FindBy(xpath = "//textarea[@name='cc']")
    private WebElement addUserInCopyInput;

    @FindBy(xpath = "//input[@class='aoT']")
    private WebElement subjectInput;

    @FindBy(xpath = "//div[@class='Am Al editable LW-avf']")
    private WebElement lettelInput;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO T-I-atl L3']")
    private WebElement sendButton;

    @FindBy(id = "link_vsm")
    private WebElement viewLetterLink;

    @FindBy(css = "td div[role=\"link\"]")
    private WebElement firstUnreadMessage;

    public LoggedInPage clickWriteButton() {
        visibilityOfElementWait(writeButton, 10);
        click(writeButton, "Click Write Button");
        return this;
    }

    public LoggedInPage typeDestination(String destination) {
        visibilityOfElementWait(destinationInput, 10);
        clearAndSendKeys(destinationInput, destination, "Type Destination: " + destination);
        return this;
    }

    public LoggedInPage clickAddUserInCopySpan() {
        click(addUserInCopySpan, "Click Add User In Copy Span");
        return this;
    }

    public LoggedInPage typeUserInCopyEmail(String email) {
        clearAndSendKeys(addUserInCopyInput, email, "Type User in Copy Email: " + email);
        return this;
    }

    public LoggedInPage addUserInCopy(String email) {
        clickAddUserInCopySpan();
        typeUserInCopyEmail(email);
        return this;
    }

    public LoggedInPage typeSubject(String subject) {
        clearAndSendKeys(subjectInput, subject, "Type Subject: " + subject);
        return this;
    }

    public LoggedInPage typeLetter(String letter) {
        clearAndSendKeys(lettelInput, letter, "Type letter: " + letter);
        return this;
    }

    public LoggedInPage clickSendEmailButton() {
        click(sendButton, "Click Send email button");
        return this;
    }

    public SentMessagesPage clickViewLetter() {
        visibilityOfElementWait(viewLetterLink, 10);
        click(viewLetterLink, "Click View Letter Link");
        return new SentMessagesPage();
    }

    public SentMessagesPage clickFirstUnreadMessage(){
        click(firstUnreadMessage, "Click First Unread Message");
        return new SentMessagesPage();
    }
}
