package com.gmail.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.qatestlab.reporter.Reporter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.*;

/**
 * Created by apuhno on 17.06.2016.
 */
public class SentMessagesPage extends BasePage {

    public SentMessagesPage() {
        PageFactory.initElements(BaseTest.driver(), this);
    }

    @FindAll({
            @FindBy(xpath = "//div[@role='listitem']")
    })
    private List<WebElement> sentMessagesList;

    public String getLastMessageText(){
        List<String[]> texts = new ArrayList<>();
        for (int i=0; i<sentMessagesList.size(); i++) {
            texts.add(sentMessagesList.get(i).getText().split("\n"));
        }
        String lastMessage = texts.get(texts.size()-1)[3];
        Reporter.Log("Last Message: " + lastMessage);
        return lastMessage;
    }
}
