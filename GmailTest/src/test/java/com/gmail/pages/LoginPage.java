package com.gmail.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by apuhno on 17.06.2016.
 */
public class LoginPage extends BasePage {

    public LoginPage() {
        PageFactory.initElements(BaseTest.driver(), this);
    }

    @FindBy(id = "Email")
    private WebElement emailInput;

    @FindBy(id = "next")
    private WebElement nextButton;

    @FindBy(id = "Passwd")
    private WebElement passwordInput;

    @FindBy(id = "signIn")
    private WebElement signInButton;

    public LoginPage typeEmail(String email){
        clearAndSendKeys(emailInput, email, "Type email: " + email);
        return this;
    }

    public LoginPage clickNextButton(){
        click(nextButton, "Click Next Button");
        return this;
    }

    public LoginPage typePass(String pass){
        visibilityOfElementWait(passwordInput, 1);
        clearAndSendKeys(passwordInput, pass, "Type password: " + pass);
        return this;
    }

    public LoggedInPage clickSignInButton(){
        click(signInButton, "Click SignIn Button");
        return new LoggedInPage();
    }

    public LoggedInPage login(String email, String pass){
        this.typeEmail(email);
        this.clickNextButton();
        this.typePass(pass);
        this.clickSignInButton();
        return new LoggedInPage();
    }
}
