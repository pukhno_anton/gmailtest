package com.gmail.tests;

import com.gmail.pages.LoggedInPage;
import com.gmail.pages.LoginPage;
import com.gmail.pages.SentMessagesPage;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.qatestlab.staticProvider.StaticProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

/**
 * Created by apuhno on 17.06.2016.
 */
public class GmailTest extends BaseTest {

    final String startUrl = "https://accounts.google.com/ServiceLogin?sacu=1&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&hl=en&service=mail#identifier";
    String letter = UUID.randomUUID().toString();

    @Test(dataProvider = "firstAccount", dataProviderClass = StaticProvider.class)
    public void firstUserSendEmailTest(String firstMail, String firstPass, String secondMail, String secondPass, String thirdMail, String thirdPass) throws InterruptedException {
        LoginPage loginPage = new LoginPage();
        loginPage.deleteCookies();
        loginPage.navigateToUrl(startUrl);
        LoggedInPage loggedInPage = loginPage.login(firstMail, firstPass);
        loggedInPage.clickWriteButton();
        loggedInPage.typeDestination(secondMail);
        loggedInPage.addUserInCopy(thirdMail);
        loggedInPage.typeSubject("This Is my Subject");
        loggedInPage.typeLetter(letter);
        loggedInPage.clickSendEmailButton();
        SentMessagesPage sentMessagesPage = loggedInPage.clickViewLetter();

        Assert.assertEquals(sentMessagesPage.getLastMessageText(), letter);
    }

    @Test(dataProvider = "firstAccount", dataProviderClass = StaticProvider.class, dependsOnMethods = {"firstUserSendEmailTest"})
    public void asecondUserCheckEmailTest(String firstMail, String firstPass, String secondMail, String secondPass, String thirdMail, String thirdPass) throws InterruptedException {
        LoginPage loginPage = new LoginPage();
        loginPage.deleteCookies();
        loginPage.navigateToUrl(startUrl);
        LoggedInPage loggedInPage = loginPage.login(secondMail, secondPass);
        SentMessagesPage sentMessagesPage = loggedInPage.clickFirstUnreadMessage();

        Assert.assertEquals(sentMessagesPage.getLastMessageText(), letter);
        Assert.fail();
    }

    @Test(dataProvider = "firstAccount", dataProviderClass = StaticProvider.class, dependsOnMethods = {"firstUserSendEmailTest"})
    public void athirdUserCheckEmailTest(String firstMail, String firstPass, String secondMail, String secondPass, String thirdMail, String thirdPass) throws InterruptedException {
        LoginPage loginPage = new LoginPage();
        loginPage.deleteCookies();
        loginPage.navigateToUrl(startUrl);
        LoggedInPage loggedInPage = loginPage.login(thirdMail, thirdPass);
        SentMessagesPage sentMessagesPage = loggedInPage.clickFirstUnreadMessage();

        Assert.assertEquals(sentMessagesPage.getLastMessageText(), letter);
    }
}
