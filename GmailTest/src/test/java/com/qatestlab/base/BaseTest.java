package com.qatestlab.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.remote.BrowserType;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by apuhno on 17.06.2016.
 */
public class BaseTest {
    private static ThreadLocal<WebDriver> driverThread = new ThreadLocal<>();

    public static WebDriver driver() {
        return driverThread.get();
    }

    @BeforeClass
    public void setUp() throws Exception {
        driverThread.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability(System.getProperty("browser"))));
        //driver().get("https://www.gmail.com");
        driver().manage().window().maximize();
        driver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.setProperty("org.uncommons.reportng.escape-output", "false");
    }

    DesiredCapabilities capability(String browser) {
        switch (browser) {
            case BrowserType.FIREFOX: //FireFox
            {
                return DesiredCapabilities.firefox();
            }
            case BrowserType.CHROME: //Chrome
            {
                return DesiredCapabilities.chrome();
            }
            default:
                return DesiredCapabilities.firefox();
        }
    }

    @AfterClass
    public void tearDown() {
        driver().quit();
    }
}
