package com.qatestlab.base;

import com.qatestlab.reporter.Reporter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;

/**
 * Created by apuhno on 17.06.2016.
 */
public class BasePage {
    public void invisibilityOfElementWait(WebElement element, long timeOut) {
        (new WebDriverWait(BaseTest.driver(), timeOut)).until(ExpectedConditions.invisibilityOfAllElements(Arrays.asList(element)));
    }

    public void visibilityOfElementWait(WebElement element, long timeOut) {
        (new WebDriverWait(BaseTest.driver(), timeOut)).until(ExpectedConditions.visibilityOf(element));
    }

    public void clearAndSendKeys(WebElement element, String text, String message) {
        Reporter.Log(message);
        element.clear();
        element.sendKeys(text);
    }

    public void click(WebElement element, String message) {
        Reporter.Log(message);
        element.click();
    }

    public void deleteCookies() {
        BaseTest.driver().manage().deleteAllCookies();
    }

    public void navigateToUrl(String url) {
        BaseTest.driver().get(url);
    }

    public void scrollDown() {
        ((JavascriptExecutor) BaseTest.driver()).executeScript("window.scrollTo(0,document.body.scrollHeight);");
    }

    public Boolean validateDisplayedElement(WebElement element) {
        if (element.isDisplayed() == true)
            return true;
        else
            return false;
    }

    public void scrollToBottom(WebElement element, String message) {
        Reporter.Log(message);
        do {
            scrollDown();
            if (validateDisplayedElement(element))
                break;
        } while (true);
    }
}
