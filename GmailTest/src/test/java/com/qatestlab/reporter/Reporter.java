package com.qatestlab.reporter;

import com.qatestlab.base.BaseTest;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by apuhno on 17.06.2016.
 */
public class Reporter {
    public static void Log(String message){
        org.testng.Reporter.log("[" + new SimpleDateFormat("d.M.yyyy H:mm:ss.SSS").format(new Date()) + "] " + message + "<br/>");
    }

    private static File captureElementBitmap(WebDriver driver) {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        return screen;
    }

    public static void saveScreenShot(){
        String filename = UUID.randomUUID().toString() + ".png";
        File file = new File(System.getProperty("image.dir") + filename);
        try
        {
            FileUtils.copyFile(Reporter.captureElementBitmap(BaseTest.driver()), file);
        }catch (Exception e){
            e.printStackTrace();
        }
        Reporter.Log("<img src=\"" + filename + "\"></img>");
    }
}
