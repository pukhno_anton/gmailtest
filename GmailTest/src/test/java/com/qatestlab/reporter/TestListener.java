package com.qatestlab.reporter;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by apuhno on 17.06.2016.
 */
public class TestListener extends TestListenerAdapter {
    @Override
    public void onTestFailure(ITestResult tr) {
        Reporter.saveScreenShot();
        org.testng.Reporter.setCurrentTestResult(tr);
    }
}
